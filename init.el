;(setq icon-map-list '(x-gtk-stock-map))

(package-initialize)
(use-package ivy
  :config
  (ivy-mode)
  (counsel-mode))

(use-package company
  :config
  (global-company-mode))

(use-package recentf
  :bind ("C-x C-r" . 'recentf-open-files)
  :config
  (recentf-mode 1)
  (setq recentf-max-menu-items 25)
  (setq recentf-max-saved-items 25))

					;(global-set-key "\C-x\ \C-r" 'recentf-open-files) 

(use-package tex
  :defer t
  :config
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq-default TeX-master nil)
  (setq-default preview-auto-cache-preamble t)
  (setq reftex-plug-into-AUCTeX t)
  (setq-default TeX-engine 'default)
  (setq TeX-PDF-mode t)
  (setq cdlatex-paired-parens "($[{")
  :hook
  (LaTeX-mode . (lambda ()
		  (cdlatex-mode)
		  (reftex-mode)
		  (flyspell-mode)
		  (ispell-change-dictionary "british")
		  (LaTeX-math-mode)
		  (auto-fill-mode)
		  (turn-on-latex-goodies)
		  (my-correct-indentation))))


(defun get-maxima ()
  (autoload #'maxima-mode "maxima" "Maxima mode" t)
  (autoload #'maxima "maxima" "Maxima CAS" t)
  (add-to-list 'auto-mode-alist '("\\.maxima" . maxima-mode)))

(defun turn-on-goodies ()
  (show-paren-mode t)
  (electric-pair-mode t)
  (blink-cursor-mode -1)
  ;(require 'package)
  ;(global-company-mode)
					;(yas-global-mode)
  (add-to-list 'package-archives '("melpa"
				   . "https://melpa.org/packages/") t)
  (load-theme #'nord t)
  (get-maxima))


;; (defun my-turn-on-recentf ()
;;   (recentf-mode 1)
;;   (setq recentf-max-menu-items 25)
;;   (setq recentf-max-saved-items 25)
;;   (global-set-key "\C-x\ \C-r" 'recentf-open-files))


(defun turn-on-after-init-config ()
  (add-to-list 'load-path (expand-file-name "~/.emacs.d/elisp"))
  ;(add-hook 'latex-mode-hook 'my-latex-goodies-hook)
  ;(add-hook 'LaTeX-mode-hook 'my-latex-goodies-hook)
  (setq inhibit-startup-screen t)
  (scroll-bar-mode -1)
  (tool-bar-mode -1)
  (menu-bar-mode 1)
					;(my-turn-on-recentf)
  )


(defun my-correct-indentation ()
  (setq LaTeX-indent-environment-list '(("verbatim" current-indentation)
					("verbatim*" current-indentation)
					("tabular" LaTeX-indent-tabular)
					("tabular*" LaTeX-indent-tabular)
					("align" current-indentation)
					("align*" current-indentation)
					("array" current-indentation)
					("eqnarray" current-indentation)
					("eqnarray*" current-indentation)
					("displaymath")
					("equation")
					("equation*")
					("picture")
					("tabbing"))))

(defun my-latex-setup-make ()
  (add-to-list 'TeX-command-list '("Make project" "" TeX-run-compile nil t :help "Run make")))

(defun my-latex-preview-hack ()
  "This is a hack used because preview latex will break if used with a
  dark backgroun"
  (custom-set-faces 
   '(preview-reference-face ((t (:background "#ebdbb2" :foreground
  "black")))))) 

(defun turn-on-latex-goodies ()
  ;(require 'turn-on-synctex "synctex.el")
  ;(ispell-change-dictionary "british")
  ;(turn-on-reftex)
  ;(setq reftex-plug-into-AUCTeX t)
  ;(setq reftex-section-levels (append '(("begin{frame}" . -3) )
  ;				      reftex-section-levels))

  ;; (setq reftex-section-levels '(("part" . 0)
  ;;                 ("chapter" . 1)
  ;;                 ("section" . 2)
  ;;                 ("subsection" . 3)
  ;;                 ("subsubsection" . 4)
  ;;                 ("paragraph" . 5)
  ;;                 ("subparagraph" . 6)
  ;;                 ("frametitle" . 7)
  ;;                 ("addchap" . -1)
  ;;                 ("addsec" . -2)))
  
  ;(turn-on-synctex)
  ;(turn-on-flyspell)
  ;(turn-on-cdlatex)
					;(setq cdlatex-paired-parens "($[{")
  ;(auto-fill-mode 1)
    ;(local-set-key (kbd "<C-tab>") 'LaTeX-indent-line)
  ;(TeX-engine-set 'xetex)
  ;(flymake-mode)
					;(modify-syntax-entry ?$ ".")
  ;(setq preview-auto-cache-preamble t)
  ;(my-correct-indentation)
					;(my-latex-preview-hack)
  )

(add-hook 'after-init-hook 'turn-on-after-init-config)
(add-hook 'emacs-startup-hook 'turn-on-goodies)

;; (defun remap-reftex-keys ()
;;   "Reftex shortcuts are thought for an english keyboard.
;;    This will remap the most common shortcuts to the keys 
;;    physically in the same position in a latin-american keyboard")

;(eval-after-load "reftex-mode"
;  '(progn
;     (define-key reftex-mode-map (kbd "C-c ´") 'reftex-citation)))



;; (use-package latex
;;   :defer t
;;   :config
;;   (use-package cdlatex
;;     :config
;;     (cdlatex-mode)))


;(setq TeX-auto-save t)
;(setq TeX-parse-self t)
;(setq-default TeX-master nil)
;(setq-default preview-auto-cache-preamble t)
;; (add-hook 'LaTeX-mode-hook 'auto-fill-mode)
;; (add-hook 'LaTeX-mode-hook 'flyspell-mode)
;(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
;; (add-hook 'LaTeX-mode-hook 'turn-on-reftex)
;; (add-hook 'LaTeX-mode-hook 'turn-on-cdlatex)
;; (add-hook 'LaTeX-mode-hook 'turn-on-latex-goodies)
;; (setq reftex-plug-into-AUCTeX t)
;; (setq-default TeX-engine 'xetex)
;; (setq TeX-PDF-mode t)
;; (setq cdlatex-paired-parens "($[{")


(defun customize-ebib-strip-leading-colon (fname storep)
  "Database filenames by Mendeley have this format
  
  :/absolute/path/1.pdf;:/absolute/path/2.pdf:...

  This function extracts the first path"
  (let ((idx (string-match ";" fname)))
    (if (eq idx nil) 
	(store-substring (substring fname 0 -4) 0 "/")
      (store-substring (substring fname 0 (- idx 4)) 0 "/"))))



(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(TeX-source-correlate-mode t)
 '(TeX-source-correlate-start-server t)
 '(TeX-view-program-selection
   (quote
    (((output-dvi has-no-display-manager)
      "dvi2tty")
     ((output-dvi style-pstricks)
      "dvips and gv")
     (output-dvi "xdvi")
     (output-pdf "Zathura")
     (output-html "xdg-open"))))
 '(ansi-color-names-vector
   ["#0a0814" "#f2241f" "#67b11d" "#b1951d" "#4f97d7" "#a31db1" "#28def0" "#b2b2b2"])
 '(custom-safe-themes
   (quote
    ("82358261c32ebedfee2ca0f87299f74008a2e5ba5c502bde7aaa15db20ee3731" "d91ef4e714f05fff2070da7ca452980999f5361209e679ee988e3c432df24347" "a8245b7cc985a0610d71f9852e9f2767ad1b852c2bdea6f4aadc12cce9c4d6d0" "725a0ac226fc6a7372074c8924c18394448bb011916c05a87518ad4563738668" "d3691a8307e87d4289d7e8057eb1a5ac55ef2f009df9c0309c35262c43b1c7b8" "0598c6a29e13e7112cfbc2f523e31927ab7dce56ebb2016b567e1eff6dc1fd4f" "bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476" "f633d825e380caaaefca46483f7243ae9a663f6df66c5fad66d4cab91f731c86" "274fa62b00d732d093fc3f120aca1b31a6bb484492f31081c1814a858e25c72e" default)))
 '(ebib-bib-search-dirs (quote ("~/Library/Bibtex")))
 '(ebib-file-associations (quote (("pdf" . "zathura") ("ps" . "gv"))))
 '(ebib-file-name-mod-function (quote customize-ebib-strip-leading-colon))
 '(hl-todo-keyword-faces
   (quote
    (("TODO" . "#dc752f")
     ("NEXT" . "#dc752f")
     ("THEM" . "#2d9574")
     ("PROG" . "#4f97d7")
     ("OKAY" . "#4f97d7")
     ("DONT" . "#f2241f")
     ("FAIL" . "#f2241f")
     ("DONE" . "#86dc2f")
     ("NOTE" . "#b1951d")
     ("KLUDGE" . "#b1951d")
     ("HACK" . "#b1951d")
     ("TEMP" . "#b1951d")
     ("FIXME" . "#dc752f")
     ("XXX+" . "#dc752f")
     ("\\?\\?\\?+" . "#dc752f"))))
 '(menu-bar-mode nil)
 '(package-selected-packages
   (quote
    (edwin edwina nord-theme flymake-racket racket-mode eclipse-theme ebib magit pdf-tools solarized-theme spacemacs-theme gotham-theme cdlatex company-reftex rebecca-theme counsel lua-mode ein dracula-theme use-package)))
 '(pdf-view-midnight-colors (quote ("#b2b2b2" . "#292b2e")))
 '(tool-bar-mode nil))

(put 'upcase-region 'disabled nil)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(preview-reference-face ((t (:background "#ebdbb2" :foreground "black")))))
(put 'downcase-region 'disabled nil)
